package;

import type.Node;

class Machine {
    var array:Array<Int>;
    var funcArray:Array<Array<Node>>;
    var index:Int;
    var printFunc:String -> Void;
    var inputFunc:() -> String;

    function new(printFunc:(String) -> Void, inputFunc:() -> String) {
        array = [0];
        funcArray = [];
        index = 0;
        this.printFunc = printFunc;
        this.inputFunc = inputFunc;
    }

    public static function runAll(nodes:Array<Node>, print:String -> Void, input:() -> String) {
        final machine = new Machine(print, input);
        machine.run(nodes);
    }

    function run(nodes:Array<Node>) {
        for (node in nodes) {
            switch (node) {
                case PointerInc:
                    pointerInc();
                case PointerDec:
                    pointerDec();
                case ValueInc:
                    valueInc();
                case ValueDec:
                    valueDec();
                case Print:
                    print();
                case Input:
                    input();
                case While(nodes):
                    while_(nodes);
                case Func(nodes):
                    function_(nodes);
                case Call:
                    call();
            }
        }
    }

    function pointerInc() {
        index ++;
        if (array.length == index) {
            array.push(0);
        }
    }

    function pointerDec() {
        if (index == 0) {
            throw 'index cannot be under 0';
        }
        index --;
    }

    function valueInc() {
        array[index] ++;
        if (array[index] == 256) {
            array[index] = 0;
        }
    }

    function valueDec() {
        array[index] --;
        if (array[index] == -1) {
            array[index] = 255;
        }
    }

    function print() {
        printFunc(String.fromCharCode(array[index]));
    }

    function input() {
        array[index] = inputFunc().charCodeAt(0);
    }

    function while_(nodes:Array<Node>) {
        while (array[index] != 0) {
            run(nodes);
        }
    }

    function function_(nodes:Array<Node>) {
        funcArray.push(nodes);
    }

    function call() {
        if (array[index] >= funcArray.length) {
            throw 'function array index over range';
        }
        final func = funcArray[array[index]];
        run(func);
    }
}