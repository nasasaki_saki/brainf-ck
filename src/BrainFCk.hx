package;

#if MAIN_BUILD
import python.lib.Sys;
#end

class BrainFCk {
    public static function run(code:String, print:String->Void, input:() -> String) {
        Machine.runAll(Parser.parse(Tokenizer.tokenize(code)), print, input);
    }

    #if MAIN_BUILD
    public static function main() {
        run('
            function 0 {
                引数2(a2)の関数を引数1(a1)だけ実行した和を返す

                >>>[-]>[-]<<
                <[->
                    [->+>+<<]>>;
                    [-<<<<+>>>>]
                    <[-<+>]
                    <
                <]
                <
            }

            function 1 {
                10を返す
                +++++++++
            }

            [-]>++++++>+<<;+++++.+.+.
        ', Sys.stdout.write, () -> Sys.stdin.read(1));
    }
    #end
}
