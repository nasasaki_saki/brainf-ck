package;

import type.Node;
import type.Token;

class Parser {
    var tokens:Array<Token>;
    var nextIndex:Int;

    function new() {

    }

    public static function parse(tokens:Array<Token>) {
        final parser = new Parser();
        parser.tokens = tokens;
        parser.nextIndex = 0;
        return parser.node();
    }

    function node():Array<Node> {
        final result:Array<Node> = [];

        var i = nextIndex;
        while (i < tokens.length) {
            nextIndex ++;
            switch (tokens[i]) {
                case PointerInc:
                    result.push(PointerInc);
                case PointerDec:
                    result.push(PointerDec);
                case ValueInc:
                    result.push(ValueInc);
                case ValueDec:
                    result.push(ValueDec);
                case Print:
                    result.push(Print);
                case Input:
                    result.push(Input);
                case WhileStart:
                    result.push(While(node()));
                case WhileEnd:
                    return result;
                case FuncStart:
                    result.push(Func(node()));
                case FuncEnd:
                    return result;
                case Call:
                    result.push(Call);
            }
            i = nextIndex;
        }
        return result;
    }
}