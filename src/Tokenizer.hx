package ;

import type.Token;

class Tokenizer {
    public static function tokenize(code:String):Array<Token> {
        final tokens:Array<Token> = [];

        for (i in 0...code.length) {
            final c:Token = cast code.charAt(i);
            switch (c) {
                case PointerInc,
                    PointerDec,
                    ValueInc,
                    ValueDec,
                    Print,
                    Input,
                    WhileStart,
                    WhileEnd,
                    FuncStart,
                    FuncEnd,
                    Call:
                        tokens.push(c);
                case _:
            }
        }

        return tokens;
    }
}