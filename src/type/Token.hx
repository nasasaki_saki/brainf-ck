package type;

@:enum abstract Token(String) {
    var PointerInc = '>';
    var PointerDec = '<';
    var ValueInc = '+';
    var ValueDec = '-';
    var Print = '.';
    var Input = ',';
    var WhileStart = '[';
    var WhileEnd = ']';
    var FuncStart = '{';
    var FuncEnd = '}';
    var Call = ';';
}
