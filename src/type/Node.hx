package type;

enum Node {
    PointerInc;
    PointerDec;
    ValueInc;
    ValueDec;
    Print;
    Input;
    While(nodes: Array<Node>);
    Func(nodes: Array<Node>);
    Call;
}